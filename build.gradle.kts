import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	kotlin("jvm") version "1.6.21"
	java
	application
}

sourceSets.main {
	java.srcDirs("src/main/")
}

application {
	mainClass.set("com.github.doomsdayrs.atlauncherx.flatpak.MainKt")
}

group = "com.github.doomsdayrs.atlauncherx-flatpak"
version = "0.1.0"

repositories {
	mavenCentral()
}

dependencies {
	testImplementation(kotlin("test"))
	implementation("com.github.hypfvieh:dbus-java:3.3.1")
}

tasks.jar {
	manifest.attributes["Main-Class"] = application.mainClass.get()
	manifest.attributes["Main-Class"] = application.mainClass.get()
	archiveVersion.set("")
	val dependencies = configurations
		.runtimeClasspath
		.get()
		.map(::zipTree)
	from(dependencies)

	exclude("jni/x86_64-Windows/")
	exclude("jni/x86_64-SunOS/")
	exclude("jni/x86_64-OpenBSD/")
	exclude("jni/x86_64-FreeBSD/")
	exclude("jni/x86_64-DragonFlyBSD/")
	exclude("jni/sparcv9-SunOS/")
	exclude("jni/ppc-AIX/")
	exclude("jni/ppc64-AIX/")
	exclude("jni/i386-Windows/")
	exclude("jni/i386-SunOS/")
	exclude("jni/Darwin/")

	duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}

tasks.test {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions.jvmTarget = "11"
}